package com.example.demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.SpringApplication;

import com.tmax.proobject.runtime.ProObjectServletInitializer;

@Configuration
@SpringBootApplication
@ComponentScan(basePackages = {"com.tmax.proobject"})
public class ProObjectAppliction extends ProObjectServletInitializer{

	public static void main(String[] args) {
        SpringApplication.run(ProObjectAppliction.class, args);
    }
}
