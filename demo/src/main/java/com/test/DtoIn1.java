package com.test;

import com.tmax.proobject.core2.annotation.DataObject;
import java.io.Serializable;
import java.lang.Cloneable;

@javax.annotation.Generated(
	value = "com.tmaxsoft.sts4.codegen.dto.DtoGenerator",
	date= "23. 2. 15. 오후 2:23"
)

@DataObject
public class DtoIn1 implements Serializable, Cloneable
{
    
    private static final long serialVersionUID = 1L;
    
    public String getLogicalName() {
    	return "DtoIn1";
    }
    
    private String AAA = null;
    
    public String getAAA() {
    	return AAA;
    }	
    
    public void setAAA(String AAA) {
    	if (AAA == null) {
    		this.AAA = null;
    	} else {
    		this.AAA = AAA;
    	}
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	
    	buffer.append("AAA : ").append(AAA).append("\n");   
    	return buffer.toString();
    }
    
    @Override
    public Object clone() {
    	DtoIn1 copyObj = new DtoIn1();
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(Object _dtoIn1){
    	if (this == _dtoIn1)
    		return;
    		
    	DtoIn1 __dtoIn1 = (DtoIn1) _dtoIn1;
    	this.setAAA(__dtoIn1.getAAA());
    }
}

