package com.tmax.dto;

import com.tmax.proobject.core2.annotation.DataObject;
import java.io.Serializable;
import java.lang.Cloneable;

@javax.annotation.Generated(
	value = "com.tmaxsoft.sts4.codegen.dto.DtoGenerator",
	date= "23. 2. 15. 오후 2:22"
)

@DataObject
public class EmpDTO implements Serializable, Cloneable
{
    
    private static final long serialVersionUID = 1L;
    
    public String getLogicalName() {
    	return "EmpDTO";
    }
    
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	
    	return buffer.toString();
    }
    
    @Override
    public Object clone() {
    	EmpDTO copyObj = new EmpDTO();
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(Object _empDTO){
    	if (this == _empDTO)
    		return;
    		
    	EmpDTO __empDTO = (EmpDTO) _empDTO;
    }
}

