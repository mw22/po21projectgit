package com.tmax.service;

import org.springframework.stereotype.Service;
import com.tmax.proobject.runtime.system.service.dto.EmptyDataObject;

@javax.annotation.Generated(
    value = "com.tmaxsoft.sts4.codegen.service.ServiceGenerator",
    date= "23. 2. 15. 오후 2:22",
    comments= "Service3"
)
@Service
public class Service3{
    
    public EmptyDataObject method1(EmptyDataObject input) throws Throwable {
    	System.out.println("Hello world");
        return null;
    }
    
}

